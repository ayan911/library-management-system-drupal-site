<?php
function library_custom_rules_action_info()
{
	return array(
		'library_custom_rules_action_issue_book' => array(
			'label' => 'Issue Book',
			'group' => 'Custom',
			'parameter' => array(
				'book' => array(
					'type' => 'node',
					'label' => 'Book',
					'description' => 'Name of the book which will be issued',
				),
				'user' => array(
					'type' => 'integer',
					'label' => 'User',
					'description' => 'Name of the user whom the book will be issued',
				),
			),
		),
		'library_custom_rules_action_add_stock' => array(
			'label' => 'Add Stock',
			'group' => 'Custom',
			'parameter' => array(
				'book' => array(
					'type' => 'node',
					'label' => 'Book ID',
					'description' => 'Name of the book which will be returned',
				),
				'stock' => array(
					'type' => 'integer',
					'label' => 'stock',
					'description' => 'Amount of stock to be added',
				),
			),
		),
		'library_custom_rules_action_delete_stock' => array(
			'label' => 'Delete Stock',
			'group' => 'Custom',
			'parameter' => array(
				'book' => array(
					'type' => 'node',
					'label' => 'Book ID',
					'description' => 'Name of the book which will be returned',
				),
				'stock' => array(
					'type' => 'integer',
					'label' => 'stock',
					'description' => 'Amount of stock to be deleted',
				),
			),
		),
		'library_custom_rules_action_return_book' => array(
			'label' => 'Return Book',
			'group' => 'Custom',
			'parameter' => array(
				'issue' => array(
					'type' => 'node',
					'label' => 'Issue/Return',
					'description' => 'Name of the book which will be returned',
				),
			),
		),
	);
}

function library_custom_rules_action_issue_book($book,$user_id){
	$bid = $book->vid;
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type','node')
		->entityCondition('bundle','issue_return')
		->fieldCondition('field_status','value','Issued','=')
		->fieldCondition('field_book','target_id',$bid,"=")
		->fieldCondition('field_user','target_id',$user_id,"=");
		$result = $query->execute();
		$stock_node = entity_load("node",array($result['node'][36]->nid));
			$node = new stdClass();
	        $node->type = "issue_return";  //content type
	        node_object_prepare($node);
	        $node->language = LANGUAGE_NONE; // Or e.g. 'en' if locale is enabled
	        //$node->uid = $result->user->uid;   //author of the node
	        $node->field_book['und'][0]['target_id'] = $bid;
			$node->field_user['und'][0]['target_id'] = $user_id;  //entity reference field
	        $node->field_status['und'][0]['value'] = 'Issued';
			$node->field_issue_date['und'][0]['value']=gmdate('Y-m-d H:i:s');
			$node->field_return_date['und'][0]['value'] = gmdate('Y-m-d H:i:s', strtotime("+7 days"));//normal field
			$node->field_fine['und'][0]['value'] = 0;
	        node_save($node);
}

function library_custom_rules_action_add_stock($book,$stock){
	if($stock<=0){
		drupal_set_message('Please enter a value more than 0','error');
	}
	$bid = $book -> vid;
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type','node')
	->entityCondition('bundle','transaction_list' );
		$result=$query->execute();
		$node = new stdClass();
		$node -> type="transaction_list";
		node_object_prepare($node);
		$node->language = LANGUAGE_NONE;
		$node -> field_book['und'][0]['target_id'] = $bid;
		$node -> field_stock['und'][0]['value'] = $stock;
		$node -> field_transaction_type['und'][0]['value'] = "Added";
		$node -> field_transaction_date['und'][0]['value'] = gmdate('Y-m-d H:i:s');
		node_save($node);
}

function library_custom_rules_action_delete_stock($book,$stock){
	if($stock<=0){
		drupal_set_message('Please enter a value more than 0','error');
	}	
	$bid = $book -> nid;
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type','node')
	->entityCondition('bundle','transaction_list')
	->fieldCondition('field_book','target_id',$bid,"=");
		$result=$query->execute();
		$stock_node = entity_load("node",array($result['node'][36]->nid));
		$node = new stdClass();
		$node->type="transaction_list";
		node_object_prepare($node);
		$node->language = LANGUAGE_NONE;
		$node->field_book['und'][0]['target_id'] = $bid;
		$node->field_stock['und'][0]['value'] = $stock * (-1);
		$node->field_transaction_type['und'][0]['value'] = "Deleted";
		$node->field_transaction_date['und'][0]['value'] = gmdate('Y-m-d H:i:s');
		node_save($node);
}

function library_custom_rules_action_return_book($issue){
	$id = $issue->nid;
	$node = entity_load('node',array($id));
	$node[$id]->field_status['und'][0]['value']="Returned";
	$date1 = strtotime($node[$id]->field_return_date['und'][0]['value']);
	$date2 = strtotime(gmdate('Y-m-d'));
	$interval = ceil(($date2 - $date1)/86400);
	if($interval>0){
		$node[$id]->field_fine['und'][0]['value'] = $interval*variable_get('fine_value');	
	}
	node_save($node[$id]);
}