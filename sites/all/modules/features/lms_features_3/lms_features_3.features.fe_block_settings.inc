<?php
/**
 * @file
 * lms_features_3.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function lms_features_3_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-user-s-nav-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-user-s-nav-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'guest' => 5,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootsub' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bootsub',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-category_list-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'category_list-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootsub' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bootsub',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
