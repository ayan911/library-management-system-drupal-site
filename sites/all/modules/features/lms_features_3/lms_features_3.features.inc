<?php
/**
 * @file
 * lms_features_3.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lms_features_3_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function lms_features_3_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function lms_features_3_node_info() {
  $items = array(
    'book' => array(
      'name' => t('Book'),
      'base' => 'node_content',
      'description' => t('The list of books'),
      'has_title' => '1',
      'title_label' => t('Book List'),
      'help' => '',
    ),
    'fine' => array(
      'name' => t('Fine'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'genre' => array(
      'name' => t('Genre'),
      'base' => 'node_content',
      'description' => t('A list of genres'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'issue_return' => array(
      'name' => t('Issue/Return'),
      'base' => 'node_content',
      'description' => t('Keeps track of book issue and return'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'transaction_list' => array(
      'name' => t('Transaction List'),
      'base' => 'node_content',
      'description' => t('list of all transactions across the system'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
