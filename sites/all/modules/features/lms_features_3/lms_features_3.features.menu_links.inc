<?php
/**
 * @file
 * lms_features_3.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function lms_features_3_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_booklist:booklist.
  $menu_links['main-menu_booklist:booklist'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'booklist',
    'router_path' => 'booklist',
    'link_title' => 'Booklist',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_booklist:booklist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_issue-list:issue-list.
  $menu_links['main-menu_issue-list:issue-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'issue-list',
    'router_path' => 'issue-list',
    'link_title' => 'Issue List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_issue-list:issue-list',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_return-list:return-list.
  $menu_links['main-menu_return-list:return-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'return-list',
    'router_path' => 'return-list',
    'link_title' => 'Return List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_return-list:return-list',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_user-issue-list:uissue-list.
  $menu_links['main-menu_user-issue-list:uissue-list'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'uissue-list',
    'router_path' => 'uissue-list',
    'link_title' => 'User Issue List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_user-issue-list:uissue-list',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_user-return-list:ureturnlist.
  $menu_links['main-menu_user-return-list:ureturnlist'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'ureturnlist',
    'router_path' => 'ureturnlist',
    'link_title' => 'User Return List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_user-return-list:ureturnlist',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-user-s-nav-menu_booklist:ubooklist.
  $menu_links['menu-user-s-nav-menu_booklist:ubooklist'] = array(
    'menu_name' => 'menu-user-s-nav-menu',
    'link_path' => 'ubooklist',
    'router_path' => 'ubooklist',
    'link_title' => 'Booklist',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-user-s-nav-menu_booklist:ubooklist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-user-s-nav-menu_my-issue-list:uissue-list.
  $menu_links['menu-user-s-nav-menu_my-issue-list:uissue-list'] = array(
    'menu_name' => 'menu-user-s-nav-menu',
    'link_path' => 'uissue-list',
    'router_path' => 'uissue-list',
    'link_title' => 'My Issue List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-user-s-nav-menu_my-issue-list:uissue-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-user-s-nav-menu_my-return-list:ureturnlist.
  $menu_links['menu-user-s-nav-menu_my-return-list:ureturnlist'] = array(
    'menu_name' => 'menu-user-s-nav-menu',
    'link_path' => 'ureturnlist',
    'router_path' => 'ureturnlist',
    'link_title' => 'My Return List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-user-s-nav-menu_my-return-list:ureturnlist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-user-s-nav-menu_my-transaction-list:utranslist.
  $menu_links['menu-user-s-nav-menu_my-transaction-list:utranslist'] = array(
    'menu_name' => 'menu-user-s-nav-menu',
    'link_path' => 'utranslist',
    'router_path' => 'utranslist',
    'link_title' => 'My Transaction List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-user-s-nav-menu_my-transaction-list:utranslist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: navigation_issue-list:issue-list.
  $menu_links['navigation_issue-list:issue-list'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'issue-list',
    'router_path' => 'issue-list',
    'link_title' => 'Issue List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_issue-list:issue-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: navigation_return-list:return-list.
  $menu_links['navigation_return-list:return-list'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'return-list',
    'router_path' => 'return-list',
    'link_title' => 'Return List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_return-list:return-list',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: navigation_transaction-list:translist.
  $menu_links['navigation_transaction-list:translist'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'translist',
    'router_path' => 'translist',
    'link_title' => 'Transaction List',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_transaction-list:translist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: navigation_viewedit-users:userlist.
  $menu_links['navigation_viewedit-users:userlist'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'userlist',
    'router_path' => 'userlist',
    'link_title' => 'View/Edit Users',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_viewedit-users:userlist',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Booklist');
  t('Home');
  t('Issue List');
  t('My Issue List');
  t('My Return List');
  t('My Transaction List');
  t('Return List');
  t('Transaction List');
  t('User Issue List');
  t('User Return List');
  t('View/Edit Users');

  return $menu_links;
}
