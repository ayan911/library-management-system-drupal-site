<?php
/**
 * @file
 * lms_features_3.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function lms_features_3_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: guest.
  $roles['guest'] = array(
    'name' => 'guest',
    'weight' => 4,
  );

  // Exported role: librarian.
  $roles['librarian'] = array(
    'name' => 'librarian',
    'weight' => 3,
  );

  return $roles;
}
