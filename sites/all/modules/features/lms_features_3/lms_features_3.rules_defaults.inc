<?php
/**
 * @file
 * lms_features_3.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function lms_features_3_default_rules_configuration() {
  $items = array();
  $items['rules_add_book_custom'] = entity_import('rules_config', '{ "rules_add_book_custom" : {
      "LABEL" : "Add Book custom",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "library_custom" ],
      "USES VARIABLES" : {
        "book" : { "label" : "Book", "type" : "node" },
        "book_stock" : { "label" : "Stock", "type" : "integer" }
      },
      "ACTION SET" : [
        { "library_custom_rules_action_add_stock" : { "book" : [ "book" ], "stock" : [ "book-stock" ] } }
      ]
    }
  }');
  $items['rules_delete_book_custom'] = entity_import('rules_config', '{ "rules_delete_book_custom" : {
      "LABEL" : "Delete Book Custom",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "library_custom" ],
      "USES VARIABLES" : {
        "book" : { "label" : "Book", "type" : "node" },
        "book_stock" : { "label" : "Stock", "type" : "integer" }
      },
      "ACTION SET" : [
        { "library_custom_rules_action_delete_stock" : { "book" : [ "book" ], "stock" : [ "book-stock" ] } }
      ]
    }
  }');
  $items['rules_delete_book_stock'] = entity_import('rules_config', '{ "rules_delete_book_stock" : {
      "LABEL" : "Delete Book Stock",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "USES VARIABLES" : {
        "book" : { "label" : "Book", "type" : "node" },
        "book_stock" : { "label" : "Stock", "type" : "integer" }
      },
      "RULES" : [ { "RULE" : { "DO" : [], "LABEL" : "Stock Check" } } ]
    }
  }');
  $items['rules_issue_book_custom'] = entity_import('rules_config', '{ "rules_issue_book_custom" : {
      "LABEL" : "Issue Book Custom",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "library_custom" ],
      "USES VARIABLES" : {
        "book" : { "label" : "Book", "type" : "node" },
        "user" : { "label" : "User", "type" : "integer" }
      },
      "ACTION SET" : [
        { "library_custom_rules_action_issue_book" : { "book" : [ "book" ], "user" : [ "user" ] } }
      ]
    }
  }');
  $items['rules_issue_book_custom_user'] = entity_import('rules_config', '{ "rules_issue_book_custom_user" : {
      "LABEL" : "Issue Book Custom User",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "library_custom" ],
      "USES VARIABLES" : { "book" : { "label" : "Book", "type" : "node" } },
      "ACTION SET" : [
        { "library_custom_rules_action_issue_book" : { "book" : [ "book" ], "user" : [ "site:current-user:uid" ] } }
      ]
    }
  }');
  $items['rules_issue_to_trans'] = entity_import('rules_config', '{ "rules_issue_to_trans" : {
      "LABEL" : "Issue to trans",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--issue_return" : { "bundle" : "issue_return" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "Issued" } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "transaction_list",
              "param_title" : "issue to transaction",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-book" ],
            "value" : [ "node:field-book" ]
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-user" ],
            "value" : [ "node:field-user" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-stock" ], "value" : "-1" } },
        { "data_set" : {
            "data" : [ "entity-created:field-transaction-type" ],
            "value" : "Issued"
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-transaction-date" ],
            "value" : [ "site:current-date" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } }
      ]
    }
  }');
  $items['rules_message_display'] = entity_import('rules_config', '{ "rules_message_display" : {
      "LABEL" : "message display",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ACTION SET" : [ { "drupal_message" : { "message" : "Successful!!" } } ]
    }
  }');
  $items['rules_message_display_rule'] = entity_import('rules_config', '{ "rules_message_display_rule" : {
      "LABEL" : "message display",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--transaction_list" : { "bundle" : "transaction_list" } },
      "DO" : [ { "component_rules_message_display" : [] } ]
    }
  }');
  $items['rules_return_to_trans'] = entity_import('rules_config', '{ "rules_return_to_trans" : {
      "LABEL" : "Return to trans",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--issue_return" : { "bundle" : "issue_return" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "Returned" } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "transaction_list",
              "param_title" : "return",
              "param_author" : [ "site:current-user" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-transaction-type" ],
            "value" : "Returned"
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-transaction-date" ],
            "value" : [ "site:current-date" ]
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-book" ],
            "value" : [ "node:field-book" ]
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-user" ],
            "value" : [ "node:field-user" ]
          }
        },
        { "data_set" : { "data" : [ "entity-created:field-stock" ], "value" : "1" } },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } }
      ]
    }
  }');
  $items['rules_return_update'] = entity_import('rules_config', '{ "rules_return_update" : {
      "LABEL" : "Return Update",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "library_custom" ],
      "ACCESS_EXPOSED" : "1",
      "USES VARIABLES" : { "issue" : { "label" : "Issue\\/Return", "type" : "node" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "issue" ], "field" : "field_status" } }
      ],
      "DO" : [
        { "library_custom_rules_action_return_book" : { "issue" : [ "issue" ] } }
      ]
    }
  }');
  return $items;
}
