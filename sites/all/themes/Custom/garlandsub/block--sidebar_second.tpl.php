<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
<div class="wrapper">
  <?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
  <h2 id="navhead" <?php print $title_attributes; ?>><?php print $block->subject ?></h2>
<?php endif;?>
  <?php print render($title_suffix); ?>

  <div class="content" <?php print $content_attributes; ?>>
    <h6><?php print $content ?></h6>
    </div>
  </div>
</div>