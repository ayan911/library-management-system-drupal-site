<?php
function garlandsub_form_system_theme_settings_alter(&$form, &$form_state){
	$form['theme_settings']['my_option'] = array(
		'#type' => 'checkbox',
		'#title' => t('My Option'),
		'#default_value' => theme_get_setting('my_option'),
	);
}
